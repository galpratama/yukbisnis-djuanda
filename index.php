<?php include "header.php" ?>    

	<!-- *****************************************************************************************************************
	 HEADERWRAP
	 ***************************************************************************************************************** -->
	<div id="top-slider" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#top-slider" data-slide-to="0" class="active"></li>
	    <li data-target="#top-slider" data-slide-to="1"></li>
	    <li data-target="#top-slider" data-slide-to="2"></li>
	  </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="assets/img/slider-1.jpg" alt="First slide">
        </div>
        <div class="item">
          <img src="assets/img/slider-1.jpg" alt="First slide">
        </div>
        <div class="item">
          <img src="assets/img/slider-1.jpg" alt="First slide">
        </div>
      </div>
      </div>
	<br>
	<div class="container">
		
	<div class="row">

	        	<div class="clearfix visible-sm"></div>

				<!-- Featured -->
	        	<div class="col-lg-12 col-md-12 col-sm-12">

	        	
					<div class="col-lg-12 col-sm-12">
	            		<div class="well">
	            		
	            		<h4>Anda ingin update otomatis produk/jasa terbaru	dari bisnis ini?
	            			<div class="pull-right"><btn class="btn-warning btn-lg"><i class="fa fa-rss"></i> Yuk Update</btn></div>
	            			</h4>
	            		</div>
	            	</div>
	        		<div class="col-lg-12 col-sm-12">
	            		<span class="title">PRODUK TERBARU
	            			<div class="pull-right"><a class="btn btn-danger btn-sm">Baru</a><a class="btn btn-success btn-sm">Second</a></div>
	            		</span>
	            	</div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p>
		                        	<h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3 col-sm-3 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php"><span class="label label-danger">&nbsp;</span></a> Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>Rp.	250.000</h4>
		                        	
		                        <p></p>
		                    </div>
		                </div>
		            </div>
	        	</div>
	        	<!-- End Featured -->

	        </div>
	        <div class="row">
	        <div class="col-lg-12 col-sm-12">
	            		<span class="title">FEATURED PRODUCTS</span>
	            	</div>
	        	<div class="list-group">
		          <a href="#" class="list-group-purchases active">
		                <div class="media col-md-3">
		                    <figure class="pull-left">
		                        <img class="media-object img-rounded img-responsive"  src="http://placehold.it/350x250" alt="placehold.it/350x250" >
		                    </figure>
		                </div>
		                <div class="col-md-6">
		                    <h4 class="list-group-item-heading"> List group heading </h4>
		                    <p class="list-group-item-text"> Qui diam libris ei, vidisse incorrupte at mel. His euismod salutandi dissentiunt eu. Habeo offendit ea mea. Nostro blandit sea ea, viris timeam molestiae an has. At nisl platonem eum. 
		                        Vel et nonumy gubergren, ad has tota facilis probatus. Ea legere legimus tibique cum, sale tantas vim ea, eu vivendo expetendis vim. Voluptua vituperatoribus et mel, ius no elitr deserunt mediocrem. Mea facilisi torquatos ad.
		                    </p>
		                </div>
		                <div class="col-md-3 text-center">
		                    <h2> 14240 <small> purchases </small></h2>
		                    <button type="button" class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart"></i> Buy</button>
		                    <div class="stars">
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star-empty"></span>
		                    </div>
		                    <p> Average 4.5 <small> / </small> 5 </p>
		                </div>
		          </a>
		          <a href="#" class="list-group-purchases">
		                <div class="media col-md-3">
		                    <figure class="pull-left">
		                        <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250" alt="placehold.it/350x250" >
		                    </figure>
		                </div>
		                <div class="col-md-6">
		                    <h4 class="list-group-item-heading"> List group heading </h4>
		                    <p class="list-group-item-text"> Eu eum corpora torquatos, ne postea constituto mea, quo tale lorem facer no. Ut sed odio appetere partiendo, quo meliore salutandi ex. Vix an sanctus vivendo, sed vocibus accumsan petentium ea. 
		                        Sed integre saperet at, no nec debet erant, quo dico incorrupte comprehensam ut. Et minimum consulatu ius, an dolores iracundia est, oportere vituperata interpretaris sea an. Sed id error quando indoctum, mel suas saperet at.                         
		                    </p>
		                </div>
		                <div class="col-md-3 text-center">
		                    <h2> 12424 <small> purchases </small></h2>
		                    <button type="button" class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart"></i> Buy</button>
		                    <div class="stars">
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star-empty"></span>
		                    </div>
		                    <p> Average 3.9 <small> / </small> 5 </p>
		                </div>
		          </a>
		          <a href="#" class="list-group-purchases" id="list-product-home">
		                <div class="media col-md-3">
		                    <figure class="pull-left">
		                        <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250" alt="placehold.it/350x250">
		                    </figure>
		                </div>
		                <div class="col-md-6">
		                    <h4 class="list-group-item-heading"> List group heading </h4>
		                    <p class="list-group-item-text"> Ut mea viris eripuit theophrastus, cu ponderum accusata consequuntur cum. Suas quaestio cotidieque pro ea. Nam nihil persecuti philosophia id, nam quot populo ea. 
		                        Falli urbanitas ei pri, eu est enim volumus, mei no volutpat periculis. Est errem iudicabit cu. At usu vocibus officiis, ad ius eros tibique appellantur.                         
		                    </p>
		                </div>
		                <div class="col-md-3 text-center">
		                    <h2> 13540 <small> purchases </small></h2>
		                    <button type="button" class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart"></i> Buy</button>
		                    <div class="stars">
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star"></span>
		                        <span class="glyphicon glyphicon-star-empty"></span>
		                    </div>
		                    <p> Average 4.1 <small> / </small> 5 </p>
		                </div>
		          </a>
		        </div>
	        </div>
	</div>

		<br><br>
	
<?php include "footer.php" ?>