<?php include "header.php" ?>    

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Product List
				<small>Djuanda Store</small></h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->
	<div class="container mt">
	<div class="row">

				<!-- Featured -->
	        	<div class="col-lg-9 col-md-9 col-sm-12">
	        		<div class="col-lg-12 col-sm-12">
	            		<span class="title">PRODUCT LIST</span>
	            	</div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-4 col-sm-4 hero-feature text-center">
		                <div class="thumbnail">
		                	<a href="details.php" class="link-p" style="overflow: hidden; position: relative;">
		                    	<img src="images/product-2.jpg" alt="" style="position: absolute; width: 250px; height: auto; max-width: none; max-height: none; left: -4px; top: 0px;">
		                	</a>
		                    <div class="caption prod-caption">
		                        <h3><a href="details.php">Age Of Wisdom Tan Graphic Tee</a></h3>
		                        <p>Vintage Levi shorts loafers Choupette street style tea-green Givenchy. </p>
		                        <p>
		                        	</p><h4>IDR	250.000</h4>
		                        	<a href="details.php" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i> Buy</a>
		                        <p></p>
		                    </div>
		                </div>
		            </div>
	        	</div>
	        	<!-- End Featured -->

	        	<div class="clearfix visible-sm"></div>

	        	<div class="col-lg-3 col-md-3 col-sm-12">

	        		<!-- Categories -->
	        		<div class="col-lg-12 col-md-12 col-sm-6">
		        		<div class="no-padding">
		            		<span class="title">CATEGORIES</span>
		            	</div>
						<div class="list-group list-categ">
							<a href="catalogue.php" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.php" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.php" class="list-group-item">Morbi leo risus</a>
							<a href="catalogue.php" class="list-group-item">Porta ac consectetur ac</a>
							<a href="catalogue.php" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.php" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.php" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.php" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.php" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.php" class="list-group-item">Dapibus ac facilisis in</a>
						</div>

						<div class="no-padding">
		            		<span class="title">CATEGORIES</span>
		            	</div>
						<div class="list-group list-categ">
							<a href="catalogue.php" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.php" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.php" class="list-group-item">Morbi leo risus</a>
							<a href="catalogue.php" class="list-group-item">Porta ac consectetur ac</a>
							<a href="catalogue.php" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.php" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.php" class="list-group-item">Dapibus ac facilisis in</a>
							<a href="catalogue.php" class="list-group-item">Vestibulum at eros</a>
							<a href="catalogue.php" class="list-group-item">Cras justo odio</a>
							<a href="catalogue.php" class="list-group-item">Dapibus ac facilisis in</a>
						</div>
					</div>
					<!-- End Categories -->

	        	</div>

	        </div>
	</div>

		<br><br>
	
<?php include "footer.php" ?>